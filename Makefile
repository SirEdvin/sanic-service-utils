check:
	pylint sanic_service_utils
	pycodestyle sanic_service_utils
	mypy sanic_service_utils --ignore-missing-imports
	restructuredtext-lint README.rst
	python setup.py checkdocs
pytest:
	pytest --cov sanic_service_utils --cov-report term-missing tests