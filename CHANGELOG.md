# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.6.3] - 2020-01-23

### Changed

- Allow to any ipython version bigger than target

## [0.6.2] - 2019-10-28

### Changed

- Logging logic

## [0.6.1] - 2019-10-28

### Changed

- Logging logic

## [0.6.0] - 2019-09-23

### Changed

- Some shell mode rework

### Fixed

- Multirun logic for background task blueprint

## [0.5.6] - 2019-06-19

### Fixed

- exception processing logic

## [0.5.5] - 2019-06-19

### Added

- `console_structlog` formatter

## [0.5.4] - 2019-06-19

### Added

- More flexable logging configuration

## [0.5.3] - 2019-06-19

### Added

- Ability to control pool settings in anji-orm

## [0.5.1] - 2019-05-17

### Fixed

- Settings usage fixed

## [0.5.1] - 2019-05-17

### Changed

- Settings usage

## [0.5.0] - 2018-05-16

### Changed

- Migrate to Pipenv

## [0.4.7]

### Fixed

- Another magic fix

## [0.4.6]

### Fixed

- Some bugs

## [0.4.5]

### Added

- `async_server_trigger` function

## [0.4.4]

### Added

- Support of anji-orm file extension

## [0.4.3]

### Added

- `workers` parameter for run command
- `async_command` decorator

## [0.4.2]

### Changed

- Add `release` and `environment` marks for sentry usage

## [0.4.1]

### Added

- Commands logic for manage.py

## [0.4.0]

### Changed

- Switch sentry integration from `sanic-sentry` to `sentry-sdk`

## [0.3.2]

### Added

- Ability to ignore ssl in aiohttp session

### Changed

- AnjiORM compitability leayer

### Removed

- RethinkDB session interface

## [0.3.1] - 2018-06-17

### Fixed

- RethinkDB session compatability

## [0.3.0] - 2018-06-11

### Added

- Some tests

### Changed

- Migrate to new anji orm version, minimal is `0.5.1`

## [0.2.5] - 2018-05-18

### Changed

- `DataTableMixin` become more logical and flexable

## [0.2.4] - 2018-05-09

### Fixed

- `Dict` and `List` usage for documentation

## [0.2.3] - 2018-05-09

### Added

- `id` field to `json_data` converation
- Some anji orm fields support in sanic-openapi

## [0.2.2] - 2018-05-08

### Added

- Ability to optional prettify for data
- Anji orm mapping for sanic-openapi

### Removed

- oauth was moved to `sanic-oauth`

## [0.2.1] - 2018-05-04

### Fixed

- Background task shutdown listener will be wait for all tasks

## [0.2.0] - 2018-05-04

### Added

- New `oauth` blueprint with utility functions
- New `token` blueprint with utility functions

### Changed

- Logging configuration now setup configuration for `sanic_service_utils` package also.
- Package configuration, remove listeners package part
- Logging configuration now understand sanic debug

## [0.1.8] - 2018-01-27

### Added

- New blueprint `sanic_session_configuration`
- RethinkDB session interface

### Changed

- Minimal sanic version changed to `v0.7.0`

## [0.1.7] - 2018-01-22

### Fixed

- Typing problem

## [0.1.6] - 2018-01-22

### Changed

- Now background tasks will be stopped before server stop

## [0.1.5] - 2018-01-17

### Changed

- Decomposite request processing to add more flexability

## [0.1.4] - 2018-01-17

### Fixed

- Problem with index getting

## [0.1.3] - 2018-01-17

### Changed

- Display infomration with magic again

## [0.1.2] - 2018-01-17

### Changed

- Now web info return raw data

## [0.1.1] - 2018-01-17

### Changed

- `DataTableMixin` now provide class methods

[Unrelesed]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.3.2...master
[0.3.2]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.2.5...v0.3.0
[0.2.5]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.2.4...v0.2.5
[0.2.4]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.2.3...v0.2.4
[0.2.3]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.2.2...v0.2.3
[0.2.2]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.8...v0.2.0
[0.1.8]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.7...v0.1.8
[0.1.7]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.6...v0.1.7
[0.1.6]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.5...v0.1.6
[0.1.5]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.4...v0.1.5
[0.1.4]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.3...v0.1.4
[0.1.3]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/SirEdvin/sanic-service-utils/compare/v0.1.0...v0.1.1
