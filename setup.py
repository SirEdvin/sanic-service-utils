#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages
from itertools import chain

with open('README.rst') as readme_file:
    readme = readme_file.read()


extras_require = {
    'jinja2': ["sanic-jinja2>=0.5.5"],
    'aiohttp': ["aiohttp>=2.3.6"],
    'sentry': ["sentry-sdk~=0.6.5"],
    'anji_orm': ["anji_orm>=0.6.0", "sanic-openapi"],
    'sessions': ["sanic-session"],
    'oauth': ["sanic-oauth>=0.1.1"],
    'commands': ["click~=7.0", "ipython>=7.1.1"],
    "configuration": ["dynaconf>=2.0.2,!=2.1.0", "python-json-logger>=0.1.11", "structlog"],
}

extras_require['all'] = list(chain(*extras_require.values()))

setup(
    name='sanic-service-utils',
    version='0.6.3',
    description="Toolkit for sanic usage",
    long_description=readme,
    author="Bogdan Gladyshev",
    author_email='siredvin.dark@gmail.com',
    url='https://gitlab.com/SirEdvin/sanic-service-utils',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "sanic>=0.8.0",
    ],
    license="MIT license",
    zip_safe=False,
    keywords='sanic rethinkdb',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
    ],
    tests_require=[],
    setup_requires=[],
    extras_require=extras_require
)
